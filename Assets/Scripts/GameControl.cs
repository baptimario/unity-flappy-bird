﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour
{
    public static GameControl instance;
    public Text scoreText;
    public GameObject gameOverText;

    private int score = 0;
    public bool gameOver = false;
    public float scrollSpeed = -1.5f;
	public int HighScore { get; protected set; }

	private int HightScore;
	private int Score {
		get {
			return score;
		}
		set {
			 {
				score = value;
				if (score > HightScore) {
					HightScore = score;
					PlayerPrefs.SetInt ("HighScore", HightScore);
				}
			}
		}
	}
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
		HighScore = PlayerPrefs.GetInt("HighScore", 0);
    }

    void Update()
    {
        if (gameOver && Input.GetMouseButtonDown(0))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void BirdScored()
    {
        if (gameOver)
            return;
        Score++;
        scoreText.text = "Score: " + Score.ToString();
    }

    public void BirdDied()
    {
        gameOverText.SetActive(true);
        gameOver = true;
    }
}